import requests
import random
import timeit
import random_workers
from sys import stdout
from celery import group

q=[]
a=[]
def random_api(_num,_max):
    return requests.get('http://www.random.org/integers/?num=%d&min=1&max=%d&col=1&base=10&format=plain&rnd=new'%(_num,_max))


def compute_r(lista):
    return random_workers.compute_random.s(lista)


def make_request():
    _max = random.randint(1,250)
    _num = random.randint(2,100)
    #fromrandom = random_api(_num,_max)#random.org api odpowiada Error: You have used your quota of random bits for today.  See the quota page for details.
    fromrandom = "10\n12\n13\n14\n1\n5\n20\n11\n" #random.org api odpowiada Error: You have used your quota of random bits for today.  See the quota page for details. 
    #_var = str(fromrandom.content)#random.org api odpowiada Error: You have used your quota of random bits for today.  See the quota page for details.
    _var = str(fromrandom)
    q.append(_var.replace('\n',' '))
    return compute_r(_var)

def requests_suite():
    results = [make_request() for i in xrange(10)]
    g = group(results)().get()
    for item in g:
	a.append(item)
    for i in xrange(len(q)):
	print str(q[i]) + '==>' + str(a[i])

print 'Ten successive runs:'
index = 0
for i in range(1, 11):
    index+=1
    print str(index) + ' %.2fs' % timeit.timeit(requests_suite, number=1)
    print
