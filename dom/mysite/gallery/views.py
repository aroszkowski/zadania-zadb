# -*- encoding: utf-8 -*-
from django.contrib.auth.models import User
from models import *
from django.contrib.auth import authenticate, login
from django.contrib import auth
import os
from django.contrib.auth.forms import *
from django.shortcuts import get_object_or_404,redirect, render, render_to_response
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from forms import *
from django.conf import settings
from django.utils import timezone
from django.template.loader import render_to_string
from tasks import *


def home(request):
    try:
        if not request.user.is_authenticated(): raise PermissionDenied
        user = User.objects.get(username=request.user)
        galleries = Gallery.objects.filter(owner = user).order_by('-date')
        return render(request,'list.html', locals())
    except PermissionDenied:
        return redirect('login')
    except Exception as ex:
        messages.add_message(request, messages.INFO, ex)
        return redirect('home')
def add_photo(request,gallery, folder):
    try:
        if not request.user.is_authenticated(): raise PermissionDenied
        if(request.method == 'POST'):
            form = PhotoForm(request.POST, request.FILES)
            if form.is_valid():
                photo = form.save(commit=False)
                folder1 = Folder.objects.get(name=folder)
                    # image = request.POST['image']
                    # temp = request.FILES
                    # photo = Photo(owner=folder1, image=image)
                photo.owner = folder1
                photo.full_clean()
                photo.save()
                return redirect('viewfolder', gallery, folder)
        else:
            form = PhotoForm()
        return render(request, 'add1.html', locals())
    except Exception as ex:
        messages.add_message(request, messages.INFO, ex)
        return redirect('addphoto', gallery, folder)
def add_zipfile(request,gallery, folder):
    try:
        if not request.user.is_authenticated(): raise PermissionDenied
        if(request.method == 'POST'):
            form = ZipUploadForm(request.POST, request.FILES)
            if form.is_valid():
                zip_file = form.clean_zipfile()
                messages.add_message(request,messages.INFO,str(unpack_zip.delay(zip_file, folder).get()))
                return redirect('addzip', gallery, folder)
        else:
            form = ZipUploadForm()
        return render(request, 'addzip.html', locals())
    except Exception as ex:
        messages.add_message(request, messages.INFO, ex)
        return redirect('addzip', gallery, folder)
def view_folder(request, gallery, folder):
    try:
        if not request.user.is_authenticated(): raise PermissionDenied
        folder1 = Folder.objects.get(name=folder)
        photos = Photo.objects.filter(owner = folder1)
        urls = list()
        for photo in photos:
            urls.append(scale(photo.image,'75x100'))
        return render(request,'listfolder.html', locals())
    except PermissionDenied:
        return redirect('login')
    except Exception as ex:
        messages.add_message(request, messages.INFO, ex)
        return redirect('home')
def view_folder_resize(request, gallery, folder, size):
    try:
        if not request.user.is_authenticated(): raise PermissionDenied
        folder1 = Folder.objects.get(name=folder)
        photos = Photo.objects.filter(owner = folder1)
        urls = list()
        for photo in photos:
            urls.append(scale(photo.image,size))
        return render_to_string('listfolderajax.html',locals())
    except PermissionDenied:
        return redirect('login')
    except Exception as ex:
        messages.add_message(request, messages.INFO, ex)
        return redirect('home')

def add_folder(request,gallery):
    try:
        if not request.user.is_authenticated(): raise PermissionDenied
        if(request.method == 'POST'):
            form = FolderForm(request.POST)
            try:
                if form.is_valid():
                    folder = form.save(commit=False)
                    gallery1 = Gallery.objects.get(name=gallery)
                    folder.owner = gallery1
                    folder.full_clean()
                    folder.save()
                    return redirect('viewgallery', gallery)
            except Exception as ex:
                messages.add_message(request, messages.INFO, ex)
                return redirect('addfolder', gallery)
        else:
            form = GalleryForm()
        return render(request, 'add.html', locals())
    except Exception as ex:
        messages.add_message(request, messages.INFO, ex)
        return redirect('addfolder', gallery)

def view_gallery(request,gallery):
    try:
        if not request.user.is_authenticated(): raise PermissionDenied
        gallery1 = Gallery.objects.get(name=gallery)
        folders = Folder.objects.filter(owner = gallery1)
        return render(request,'listgallery.html', locals())
    except PermissionDenied:
        return redirect('login')
    except Exception as ex:
        messages.add_message(request, messages.INFO, ex)
        return redirect('login')

def add_gallery(request):
    try:
        if not request.user.is_authenticated(): raise PermissionDenied
        if(request.method == 'POST'):
            form = GalleryForm(request.POST)
            try:
                if form.is_valid():
                    user = User.objects.get(username=request.user)
                    gallery = form.save(commit=False)
                    gallery.owner = user
                    gallery.full_clean()
                    gallery.save()
                    return redirect('home')
            except Exception as ex:
                messages.add_message(request, messages.INFO, ex)
                return redirect('addgallery')
        else:
            form = GalleryForm()
        return render(request, 'add.html', locals())
    except Exception as ex:
        messages.add_message(request, messages.INFO, ex)
        return redirect('login')
def signup(request):
    if(request.method == 'POST'):
        try:
            form = UserCreationForm(request.POST)
            if form.is_valid():
                username = form.clean_username()
                password = form.clean_password2()
                email = form.cleaned_data.get('email')
                user = User.objects.create_user(username=username, password=password, email=email)
                user.full_clean()
                user.save()
                message = 'Congratulations! You have successfully registered'
                messages.add_message(request, messages.INFO, message)
                return redirect('home')
        except Exception as ex :
            messages.add_message(request, messages.INFO, ex)
            return redirect('signup')
    else:
        form = UserCreationForm()
    return render(request, 'signup1.html', locals())

def login(request):
    if(request.method == 'POST'):
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            if user is not None:
                messages.add_message(request, messages.INFO, 'You have been successfully logged in' )
                auth.login(request, user)
                return redirect('home')
            else:
                messages.add_message(request, messages.INFO, 'Invalid username or password' )
                return redirect('login')
    else:
        form = AuthenticationForm()
    return render(request, 'login.html', locals())

def logout(request):
    if request.user.is_authenticated():
        auth.logout(request)
        messages.add_message(request, messages.INFO, 'You have been successfully logged out' )
    else:
        pass
    return redirect('home')
# my_apps/image/templatetags/image_tags.py
import os.path

from django import template

FMT = 'JPEG'
EXT = 'jpg'
QUAL = 100

register = template.Library()


def resized_path(path, size, method):
    "Returns the path for the resized image."

    dir, name = os.path.split(path)
    image_name, ext = name.rsplit('.', 1)
    return os.path.join(dir, '%s_%s_%s.%s' % (image_name, method, size, EXT))


def scale(imagefield, size, method='scale'):
    """
    Template filter used to scale an image
    that will fit inside the defined area.

    Returns the url of the resized image.

    {% load image_tags %}
    {{ profile.picture|scale:"48x48" }}
    """

    # imagefield can be a dict with "path" and "url" keys
    if imagefield.__class__.__name__ == 'dict':
        imagefield = type('imageobj', (object,), imagefield)

    image_path = resized_path(imagefield.path, size, method)
    if not os.path.exists(image_path):
        try:
            import Image
        except ImportError:
            try:
                from PIL import Image
            except ImportError:
                raise ImportError('Cannot import the Python Image Library.')

        image = Image.open(imagefield.path)

        # normalize image mode
        if image.mode != 'RGB':
            image = image.convert('RGB')

        # parse size string 'WIDTHxHEIGHT'
        width, height = [int(i) for i in size.split('x')]

        # use PIL methods to edit images
        if method == 'scale':
            image.thumbnail((width, height), Image.ANTIALIAS)
            image.save(image_path, FMT, quality=QUAL)

        elif method == 'crop':
            try:
                import ImageOps
            except ImportError:
                from PIL import ImageOps

            ImageOps.fit(image, (width, height), Image.ANTIALIAS
            ).save(image_path, FMT, quality=QUAL)

    return resized_path(imagefield.url, size, method)



def crop(imagefield, size):
    """
    Template filter used to crop an image
    to make it fill the defined area.

    {% load image_tags %}
    {{ profile.picture|crop:"48x48" }}

    """
    return scale(imagefield, size, 'crop')


register.filter('scale', scale)
register.filter('crop', crop)