from django.db import models
from django.contrib.auth.models import User

class Gallery(models.Model):
    name = models.CharField(max_length=40, unique=True)
    owner = models.ForeignKey(User)
    date = models.DateTimeField(auto_now_add=True)
    def __unicode__(self):
        return self.name
class Folder(models.Model):
    name = models.CharField(max_length=40, unique=True)
    owner = models.ForeignKey(Gallery)
    def __unicode__(self):
        return self.name
class Photo(models.Model):
    owner = models.ForeignKey(Folder)
    image = models.ImageField(upload_to='images/')
    def __unicode__(self):
        return self.image.name