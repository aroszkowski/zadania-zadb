from django.conf.urls import patterns, include, url
from django.conf.urls import patterns, url
from django.views.generic import TemplateView, ListView, DetailView
from django.http import HttpResponse
from models import *
from views import *
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from dajaxice.core import dajaxice_autodiscover
dajaxice_autodiscover()
from django.conf import settings
urlpatterns = patterns('',
                       url(r'^$',home, name='home'),
                       url(r'^login/',login, name='login'),
                       url(r'^logout/',logout, name='logout'),
                       url(r'^signup/',signup, name='signup'),
                       url(r'^add/', add_gallery, name='addgallery'),
                       url(r'^(?P<gallery>\w+)/$', view_gallery, name='viewgallery'),
                       url(r'^(?P<gallery>\w+)/add/$', add_folder, name='addfolder'),
                       url(r'^(?P<gallery>\w+)/(?P<folder>\w+)/$', view_folder, name='viewfolder'),
                       url(r'^(?P<gallery>\w+)/(?P<folder>\w+)/add/$', add_photo , name='addphoto'),
                       url(r'^(?P<gallery>\w+)/(?P<folder>\w+)/zip/$', add_zipfile , name='addzip'),
                       url(r'^%s/' % settings.DAJAXICE_MEDIA_PREFIX, include('dajaxice.urls')),
                       )


