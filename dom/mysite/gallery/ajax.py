from dajax.core import Dajax
from dajaxice.decorators import dajaxice_register
from views import *
from tasks import *
from models import *
from forms import *
from dajaxice.utils import deserialize_form

@dajaxice_register
def resizephotos(request, gallery, folder, size):
    dajax = Dajax()
    result = view_folder_resize(request, gallery , folder, size)
    dajax.assign('#photos', 'innerHTML', str(result))
    return dajax.json()

@dajaxice_register
def multiply(request, a, b):
    dajax = Dajax()
    result = int(a) * int(b)
    dajax.assign('#photos','innerHTML',str(result))
    return dajax.json()

@dajaxice_register
def downloadzip(request,folder):
    folder1 = Folder.objects.get(name=folder)
    photos = Photo.objects.filter(owner=folder1)
    dajax = Dajax()
    zip1 = pack_zip.delay(photos=photos).get()
    message = 'download should start automatically if not <a href="%s" style="color: black;">Click Here</a>'%zip1
    dajax.assign('#extramessage','hidden','')
    dajax.script('hide_progress();')
    dajax.redirect(zip1, delay=0)
    dajax.assign('#extramessage', 'innerHTML', message)
    return dajax.json()

@dajaxice_register
def uploadzip(request, form):
    dajax = Dajax()
    form = deserialize_form(form)
    print request
    form1 = ZipUploadForm(form)
    form2 = ZipUploadForm(request.POST,request.FILES)
    print form1.clean_zipfile()
    print form2.clean_zipfile()
    if form2.is_valid():
        dajax.remove_css_class('#my_form input', 'error')
        dajax.alert("Form is_valid(), your username is: %s" % form2.cleaned_data.get('zip_file'))
    else:
        dajax.remove_css_class('#my_form input', 'error')
        for error in form2.errors:
            print error


    return dajax.json()