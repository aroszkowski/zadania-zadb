# -*- encoding: utf-8 -*-
from django import forms
from models import Gallery, Folder, Photo
import zipfile
from StringIO import StringIO

class GalleryForm(forms.ModelForm):
    class Meta:
        model = Gallery
        fields = ('name',)
class FolderForm(forms.ModelForm):
    class Meta:
        model = Folder
        fields = ('name',)
class PhotoForm(forms.ModelForm):
    class Meta:
        model = Photo
        fields = ('image',)
 # The model that uses the zipfile (has a FileField called 'zip_file')

class ZipUploadForm(forms.Form):
    zip_file = forms.FileField()
    def clean_zipfile(self):
        the_zip_file = zipfile.ZipFile(self.cleaned_data['zip_file'])
        ret = the_zip_file.testzip()
        if ret is not None:
            raise forms.ValidationError(('Error %s')%ret)
        else:
            return self.cleaned_data['zip_file']
