from djcelery import celery
import os
import zipfile
from django.conf import settings
from models import *
import shutil



extensions = {".JPG", ".PNG", ".GIF"}

@celery.task()
def pack_zip(photos):
    if photos:
        foldername = photos[0].owner
    zippath = os.path.join(settings.MEDIA_ROOT, str(foldername)+'.zip')
    zipurl = os.path.join(settings.MEDIA_URL, str(foldername)+'.zip')
    zip1 = zipfile.ZipFile(zippath, 'w')
    for photo in photos:
        zip1.write(photo.image.path)
    return str(zipurl)



@celery.task()
def unpack_zip(_zip_file_, folder):
    image_dir = os.path.join(settings.MEDIA_ROOT, 'images')
    folder1 = Folder.objects.get(name=folder)
    with zipfile.ZipFile(_zip_file_, 'w') as zip_file:
        for item in zip_file.namelist():
                    (dirname, filename) = os.path.split(item)
                    if filename == '':
                        # directory
                        if not os.path.exists(os.path.join(image_dir , dirname)):
                            os.mkdir(os.path.join(image_dir , dirname))
                    else:
                        # file
                        if not os.path.exists(os.path.join(image_dir , dirname)):
                            os.mkdir(os.path.join(image_dir , dirname))
                        fileName, fileExtension = os.path.splitext(item)
                        if fileExtension:
                            if fileExtension.upper() in extensions:
                                source = zip_file.open(item)
                                target = file(os.path.join(image_dir, filename), "wb")
                                with source, target:
                                    shutil.copyfileobj(source, target)
                                photo = Photo(owner=folder1,image=os.path.join(image_dir, item))
                                photo.full_clean()
                                photo.save()
    zip_file.close()
    return 'File unzipped'

